## Common variables
variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "common_tags" {
  description = "Tags for all resources"
  type        = map(string)
  default = {
    Owner       = "Daniels"
    Project     = "ALB test"
    Environment = "Daniels ALB test"
  }
}

variable "ubuntu_ami_owners" {
  default = ["099720109477"]
}

variable "ubuntu_ami_filters" {
  default = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
}

## Network
variable "all_networks" {
  description = "List of ports to open"
  type        = string
  default     = "0.0.0.0/0"
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnet_cidrs" {
  type = list(string)
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
  ]
}

variable "ssh_port" {
  type    = string
  default = 22
}


## instance variables
variable "nginx_instance_config" {
  type = object({
    instance_type  = string
    allow_ports    = list(number)
    user_data_file = string
    key_pair_name  = string
  })
  default = {
    instance_type  = "t3.micro"
    allow_ports    = ["80"]
    user_data_file = "userdata.sh"
    key_pair_name  = "datkacuns-ssh-keypair"
  }
}

## ELB variables
variable "alb_config" {
  type = object({
    lb_port           = number
    lb_protocol       = string
    instance_port     = number
    instance_protocol = string
  })
  default = {
    lb_port           = 80
    lb_protocol       = "HTTP"
    instance_port     = 80
    instance_protocol = "HTTP"
  }
}
