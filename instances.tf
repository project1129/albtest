## Instances and ALB resources
resource "aws_security_group" "web_access" {
  name   = "SG-web_access"
  vpc_id = aws_vpc.main.id

  dynamic "ingress" {
    for_each = var.nginx_instance_config.allow_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = [var.all_networks]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.all_networks]
  }

  tags = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]SG-Web Access" })
}

resource "aws_instance" "nginx" {
  ami                    = data.aws_ami.latest_ubuntu.id
  instance_type          = var.nginx_instance_config.instance_type
  key_name               = var.nginx_instance_config.key_pair_name
  vpc_security_group_ids = [aws_security_group.web_access.id]
  user_data              = file(var.nginx_instance_config.user_data_file)
  subnet_id              = aws_subnet.public_subnets[0].id

  tags = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Nginx" })
}

resource "aws_alb_target_group" "tgroup" {
  name     = "Nginx-target-group"
  port     = var.alb_config.instance_port
  protocol = var.alb_config.instance_protocol
  vpc_id   = aws_vpc.main.id
  # stickiness {
  #   type = "lb_cookie"
  # }
  # Alter the destination of the health check to be the login page.
  health_check {
    path = "/"
    port = var.alb_config.instance_port
  }
}

resource "aws_alb_listener" "listener_http" {
  load_balancer_arn = aws_alb.alb.arn
  port              = var.alb_config.lb_port
  protocol          = var.alb_config.lb_protocol

  default_action {
    target_group_arn = aws_alb_target_group.tgroup.arn
    type             = "forward"
  }
}

resource "aws_lb_target_group_attachment" "nginx" {
  target_group_arn = aws_alb_target_group.tgroup.arn
  target_id        = aws_instance.nginx.id
  port             = var.alb_config.instance_port
}

resource "aws_alb" "alb" {
  name               = "Nginx-ALB"
  subnets            = aws_subnet.public_subnets.*.id
  security_groups    = [aws_security_group.web_access.id]
  internal           = false
  load_balancer_type = "application"

  tags = var.common_tags
}
