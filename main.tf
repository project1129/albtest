provider "aws" {
  region = var.region
}

data "aws_ami" "latest_ubuntu" {
  owners      = var.ubuntu_ami_owners
  most_recent = true
  filter {
    name   = "name"
    values = var.ubuntu_ami_filters
  }
}
